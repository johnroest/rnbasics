import {StyleSheet, Text, View} from 'react-native';
import ParallaxScrollView from '@/components/ParallaxScrollView';


export default function Explore() {
  return (
      <ParallaxScrollView
          headerBackgroundColor={{ light: '#A1CEDC', dark: '#1D3D47' }}>
          <View style={styles.row}>
                <Text style={[styles.customText, styles.red]}>Bouw hier de opdrachten</Text>
                <Text style={[styles.customText, styles.blue]}> en maak er een feestje van</Text>
          </View>
      </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
    },
    red: {
        color: 'red',
    },
    blue: {
        color: 'blue',
    },
    customText: {
        fontSize: 16,
        fontWeight: "600",
        fontStyle: 'italic'
    }
});
