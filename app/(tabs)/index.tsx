import {Image, StyleSheet, Platform, Button} from 'react-native';

import { HelloWave } from '@/components/HelloWave';
import ParallaxScrollView from '@/components/ParallaxScrollView';
import { ThemedText } from '@/components/ThemedText';
import { ThemedView } from '@/components/ThemedView';
import {useState} from "react";


const [tasks, setTasks] = useState([
  { id: 1, text: 'Doctor Appointment', completed: true },
  { id: 2, text: 'Meeting at School', completed: false },
]);
export default function HomeScreen() {
  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: '#A1CEDC', dark: '#1D3D47' }}>
      <ThemedView style={styles.titleContainer}>
        <ThemedText type="title">Welcome to the React Native course of John and Robert-Jan</ThemedText>
        <HelloWave />
      </ThemedView>
        <ThemedText type="defaultSemiBold">Opdrachten voor de cursus:</ThemedText>
        <ThemedText type="default">1. Render het lijstje "tasks" op het scherm</ThemedText>
        <ThemedText type="default">2. Maak een knop die een item toevoegd aan het lijstje (mag hardcoded zijn)</ThemedText>
        <ThemedText type="default">3. Maak een invoerveld (TextInput) waar je kan typen en je knop voegt dat als item toe aan het lijstje</ThemedText>
        <ThemedText type="default">4. Maak een component Card met daarin het item (geef item mee als propertie van de card).</ThemedText>
        <ThemedText type="default">5. Geef elke card een eigen Image opgehaald van https://picsum.photos/200/300.</ThemedText>
        <ThemedText type="default">Bonus. Doe een api call en vul je lijstje met deze gegevens.</ThemedText>
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 8,
  },
});
